# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 31 Aug 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""

from app_config import *

Config = {
    'base_url': Cram_BASE_URL,
    'summary_api': Cram_SUMMARY_API,
    'district_api': Cram_DISTRICT_INFO_API,
    'api_key': Cram_API_KEY
}
