# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 31 Aug 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""
import json

import requests

from Fetcher.Config import Config


class Fetcher:
    def __init__(self):
        self.config = Config
        self.base_url = self.config.get('base_url')
        self.summary_api_path = self.config.get('summary_api')
        self.district_api_path = self.config.get('district_api')
        self.summary_api = '{}{}'.format(self.base_url, self.summary_api_path)
        self.district_api = '{}{}'.format(self.base_url, self.district_api_path)

    def get_headers(self):
        self.headers = dict()
        self.headers['Content-Type'] = 'application/json'
        self.headers['x-api-key'] = self.config.get('api_key')
        return self.headers

    def fetch_summary(self):
        try:
            r = requests.get(url=self.summary_api, headers=self.get_headers())
            if r.status_code == 200:
                data = json.loads(r.text)
                return 1, data
            else:
                print('Invalid Response')
                return 0, []
        except:
            print('Network Error')
            return 0, []

    def fetch_district_info(self):
        try:
            r = requests.get(url=self.district_api, headers=self.get_headers())
            if r.status_code == 200:
                data = json.loads(r.text)
                return 1, data
            else:
                print('Invalid Response')
                return 0, []
        except:
            print('Network Error')
            return 0, []
