# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 31 Aug 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""

import timeit

from Fetcher.Fetcher import Fetcher
from Models.District import District


def fetch_district_info():
    fetcher = Fetcher()
    sts, data = fetcher.fetch_district_info()
    if sts:
        s_time = timeit.default_timer()
        District.insert_district_info(data)
        elapsed_time = round(timeit.default_timer() - s_time, 3)
        print('Total Time Taken for District Info:{} Sec'.format(elapsed_time))


if __name__ == '__main__':
    fetch_district_info()
