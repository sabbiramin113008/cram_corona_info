# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 31 Aug 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""
import timeit

from Fetcher.Fetcher import Fetcher
from Models.DailySummary import DailySummary


def fetch_summary():
    fetcher = Fetcher()
    sts, data = fetcher.fetch_summary()
    if sts:
        s_time = timeit.default_timer()
        DailySummary.insert_dailySummary(data)
        elapsed_time = round(timeit.default_timer() - s_time, 3)
        print('Total Time Taken for Daily Summary:{} Sec'.format(elapsed_time))
