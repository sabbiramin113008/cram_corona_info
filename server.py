# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 31 Aug 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""

from flask import Flask, request, jsonify, send_file, make_response

from Facility.DistrictFacility import DistrictFacility
from Facility.RateLimiter import rate_limiter
from Facility.SummaryFacility import SummaryFacility
from Facility.TokenFacility import TokenFacility
from app_config import *

app = Flask(__name__)
app.secret_key = app_secret_key


@app.route('/api/v1/summaries', methods=['GET'])
@rate_limiter
def get_summaries():
    if request.method == 'GET':
        date = request.args.get('date', None)
        page = int(request.args.get('page', 1))
        size = int(request.args.get('size', 10))

        summary_facility = SummaryFacility()

        response = summary_facility.get_summaries(date=date, page=page, size=size)
        return jsonify(response), 200


@app.route('/api/v1/dashboard/summary', methods=['GET'])
@rate_limiter
def get_summary_dashboard():
    if request.method == 'GET':
        date = request.args.get('date', None)
        if not date:
            response = {
                'flag': 'ERROR',
                'message': 'No Date Specified'
            }
            return jsonify(response), 200
        summary_facility = SummaryFacility()
        sts, image_token = summary_facility.get_summary_image(date=date)

        if sts:
            image_path = r'{}\{}.jpg'.format(Image_BASE_OUTPUT_DIR_PATH, image_token)
            response = make_response(send_file(image_path, as_attachment=True))
        else:
            response = make_response()
        response.headers['Message'] = 'SUCCESS' if sts else 'ERROR'
        response.headers['Filename'] = '{}.jpg'.format(image_token) if sts else ''
        return response


@app.route('/api/v1/dashboard/district', methods=['GET'])
@rate_limiter
def get_district_info_dashboard():
    if request.method == 'GET':
        dis_code = request.args.get('district_code', None)
        if not dis_code:
            response = {
                'flag': 'ERROR',
                'message': 'No District Code Specified'
            }
            return jsonify(response), 200
        district_facility = DistrictFacility()
        sts, image_token = district_facility.get_district_image(dis_code=int(dis_code))

        if sts:
            image_path = r'D:\01personel\assignments\cram_corona_info\output\{}.jpg'.format(image_token)
            response = make_response(send_file(image_path, as_attachment=True))
        else:
            response = make_response()
        response.headers['Message'] = 'SUCCESS' if sts else 'ERROR'
        response.headers['Filename'] = '{}.jpg'.format(image_token) if sts else ''
        return response


@app.route('/api/v1/token', methods=['POST'])
def get_token():
    if request.method == 'POST':
        data = request.get_json()
        try:
            mail = data.get('mail')
        except:
            mail = None
        if not mail:
            return jsonify({'flag': 'ERROR', 'message': 'No Mail Provided'}), 200
        token_facility = TokenFacility()
        sts, token = token_facility.provide(mail=data.get('mail'))
        response = dict()
        response['flag'] = 'SUCCESS' if sts else 'ERROR'
        response['jwt_token'] = token if sts else None
        return jsonify(response), 200


if __name__ == '__main__':
    app.run(
        host=app_host,
        port=app_port,
        debug=True
    )
