# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 02 Sep 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""
import datetime
import json

import jwt

from app_config import *


class TokenFacility:
    def __init__(self):
        self.secret_key = JWT_SECRET_KEY_MASTER
        self.jwt_time_validity_in_seconds = JWT_TOKEN_EXPIARY

    def provide(self, mail):
        payload = {

            "mail": "{}".format(mail),
            "exp": datetime.datetime.utcnow() +
                   datetime.timedelta(seconds=self.jwt_time_validity_in_seconds),
        }
        jwt_token = jwt.encode(payload, self.secret_key)
        return 1, (jwt_token.decode('utf-8'))

    def get_token_value(self, token):
        try:
            de_token = jwt.decode(json.loads(token, encoding='utf-8'), self.secret_key, verify=True)
            mail = de_token.get('mail', None)
            return 1, mail
        except Exception as e:
            print('Token Invalid Error:', str(e))
            return 0, ''

