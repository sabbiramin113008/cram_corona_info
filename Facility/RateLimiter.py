# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 02 Sep 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""
import json
from functools import wraps

from flask import request

from Facility.TokenFacility import TokenFacility
from Utils.RedisUtil import RedisUtils
from app_config import *


class RateLimiter:
    def __init__(self):
        self.per_min_api_call = Rate_Limit_PER_MIN_API_CALL_COUNT
        self.per_day_api_call = Rate_Limit_PER_DAY_API_CALL_COUNT
        self.token_facility = TokenFacility()
        self.redis_util = RedisUtils()

    def check_minute_allowance(self, token):
        isValid, mail = self.token_facility.get_token_value(token=token)
        if isValid:
            hasRedisPerMin, minPayload = self.redis_util.fetch_value_expiary(key=mail, suffix='minute')
            if hasRedisPerMin:
                if minPayload.get('count') < 5:
                    new_count = int(minPayload.get('count')) + 1
                    time_left = self.redis_util.time_left(key=mail, suffix='minute')
                    if time_left:
                        print('day_min_left:', time_left)
                        payload = {'count': new_count}
                        self.redis_util.insert_value_expiary(key=mail, suffix='minute', expiary=time_left,
                                                             payload=payload)
                        return True, 'Okay'
                    return False, 'Time Out'
                return False, 'Quota Fulfilled'

            else:
                payload = {'count': 1}
                self.redis_util.insert_value_expiary(key=mail, suffix='minute', payload=payload, expiary=60)
                return True, 'Okay'
        return False, 'Token Invalid'

    def check_day_allowance(self, token):
        isValid, mail = self.token_facility.get_token_value(token=token)
        if isValid:
            hasRedisPerDay, Payload = self.redis_util.fetch_value_expiary(key=mail, suffix='day')
            if hasRedisPerDay:
                if Payload.get('count') < 100:
                    new_count = int(Payload.get('count')) + 1
                    time_left = self.redis_util.time_left(key=mail, suffix='day')
                    if time_left:
                        print('day_time_left:', time_left)
                        payload = {'count': new_count}
                        self.redis_util.insert_value_expiary(key=mail, suffix='day', expiary=time_left,
                                                             payload=payload)
                        return True, 'Okay'
                    return False, 'Time Out'
                return False, 'Quota Fulfilled'

            else:
                payload = {'count': 1}
                # todo:// Expiary Value should be changed to per day
                self.redis_util.insert_value_expiary(key=mail, suffix='day', payload=payload, expiary=86400)
                return True, 'Okay'
        return False, 'Token Invalid'

    def is_allowed(self, token):
        hasMinVal, _ = self.check_minute_allowance(token)
        hasDayVal, _ = self.check_day_allowance(token)
        if hasMinVal and hasDayVal:
            return True, ''
        elif not hasDayVal:
            return False, 'Per Day Quota Fulfilled'
        elif not hasMinVal:
            return False, 'Per Minute Quota Fulfilled'


def rate_limiter(f):
    @wraps(f)
    def decoration(*args, **kwargs):
        try:
            token = request.headers["app_token"]
        except:
            return json.dumps({"flag": "ERROR", "message": "No App Token Found"})

        token_facility = TokenFacility()
        hasToken, mail = token_facility.get_token_value(token)
        if not hasToken:
            return json.dumps({"flag": "ERROR", "message": "Invalid Token"})

        rate_limiter = RateLimiter()
        if Rate_Limit_ACTIVATE:
            print('Here in Rate Limiter Decorator')
            isAllowed, message = rate_limiter.is_allowed(token=token)

            if not isAllowed:
                return json.dumps({"flag": "ERROR", "message": message})

        return f(*args, **kwargs)

    return decoration
