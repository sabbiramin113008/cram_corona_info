# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 31 Aug 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""
from Models.District import District
from Utils.ImageBuilder import ImageBuilder


class DistrictFacility:
    def __init__(self):
        self.dis_repo = District()
        self.image_builder = ImageBuilder()

    def get_district_image(self, dis_code):
        sts, dis = self.dis_repo.get_response_by_dis_code(dis_code=dis_code)
        if sts:
            return self.image_builder.build_district_info(data=dis)

        return 0, ''
