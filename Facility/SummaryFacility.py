# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 31 Aug 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""

from Models.DailySummary import DailySummary
from Utils.ImageBuilder import ImageBuilder


class SummaryFacility:
    def __init__(self):
        self.summary_repo = DailySummary()
        self.image_builder = ImageBuilder()

    def get_summary_image(self, date):
        sts, smry = self.summary_repo.get_summary_by_date(date)
        if sts:
            return self.image_builder.build_summary(data=smry)
        return 0,''

    def get_summaries(self, date, page, size):
        response = dict()
        if date:
            sts, smry = self.summary_repo.get_summary_by_date(date=date)
            response['total_count'] = 1 if sts else 0
            if sts:
                model = {
                    'date': smry.date,
                    'confirmed_cases': smry.confirmed_cases,
                    'death_case': smry.death_case,
                    'recovery': smry.recovery,
                    'cumulative_confirmed_cases': smry.cumulative_confirmed_cases,
                    'cumulative_death': smry.cumulative_death,
                    'cumulative_recovery': smry.cumulative_recovery,
                    'daily_test': smry.daily_test,
                    'confirmed_new_cases_per_day': smry.confirmed_new_cases_per_day,
                    'total_tests': smry.total_tests,
                }
                response['items'] = [model]
                return response
            else:
                response['items'] = []
                return response
        summaries = self.summary_repo.get_summary_paginated(page=page, size=size)
        response['total_count'] = len(summaries)
        models = list()
        if len(summaries):
            for item in summaries:
                model = dict()
                model['date'] = item.date
                model['confirmed_cases'] = item.confirmed_cases
                model['death_case'] = item.death_case
                model['recovery'] = item.recovery
                model['cumulative_confirmed_cases'] = item.cumulative_confirmed_cases
                model['cumulative_death'] = item.cumulative_death
                model['cumulative_recovery'] = item.cumulative_recovery
                model['daily_test'] = item.daily_test
                model['confirmed_new_cases_per_day'] = item.confirmed_new_cases_per_day
                model['total_tests'] = item.total_tests

                models.append(model)
        response['items'] = models
        return response
