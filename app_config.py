# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 01 Sep 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""
# Image Builder and Generation

Image_EXE_WKHTMLPDF_FILE_PATH = r'C:\Program Files\wkhtmltopdf\bin\wkhtmltoimage.exe'
Image_BASE_OUTPUT_DIR_PATH = r'D:\01personel\assignments\cram_corona_info\output'

# Web App Settings

app_host = 'localhost'
app_port = 5656
app_secret_key = '!notverymuchofasecret'

# JWT Token

JWT_SECRET_KEY_MASTER = '2we3KOi#$SfkjfSFFSAS@QW!'
JWT_TOKEN_EXPIARY = 86400

# Rate Limit

Rate_Limit_PER_MIN_API_CALL_COUNT = 5
Rate_Limit_PER_DAY_API_CALL_COUNT = 100
Rate_Limit_ACTIVATE = True

# Database

Datbase_NAME = 'corona_info'
Database_USER = 'sabbir'
Database_PASSWORD = '<your-db-password>'
Database_HOST = 'localhost'
Database_PORT = 3306

Database_TIMEOUT = 15
Database_MAX_CONNECTION = 1000
Database_STALE_TIMEOUT = 15

Database_CHECK_SAME_THREAD = False

# Redis

Redis_HOST = 'localhost'
Redis_PORT = 6379
Redis_PASSWORD = ''
Redis_DB = 0
Redis_DECODE_RESPONSE = True
Redis_KEY_TEMPLATE = 'cram_'

# Cramstack

Cram_API_KEY = 'NMBDNY6-K6T4JC2-KS0AVWK-VX6FBKJ'
Cram_BASE_URL = 'https://corona-api.cramstack.com'
Cram_SUMMARY_API = '/api/v1/daily-summery'
Cram_DISTRICT_INFO_API = '/api/v1/districts'
