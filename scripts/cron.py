# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 31 Aug 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""

import time

from Jobs.DistrictJob import fetch_district_info
from Jobs.SummaryJob import fetch_summary

if __name__ == '__main__':
    interval = 3600 * 8
    while True:
        print('Starting to Execute Jobs......')
        fetch_district_info()
        fetch_summary()

        print('Entering into Sleep Mode: .......')
        time.sleep(interval)
