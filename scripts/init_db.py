# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 31 Aug 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""

from Models.DailySummary import DailySummary
from Models.District import District
from Models.PrimeModels import app_db


def init_db_sql():
    app_db.connect()
    Tables = [DailySummary, District]
    try:
        app_db.create_tables(Tables)
        print('Tables are Created')
    except Exception as e:
        print('Error: Table is Not Created: ', str(e))


if __name__ == '__main__':
    init_db_sql()
