# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 02 Sep 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""

import json
from datetime import timedelta

import redis

from app_config import *


def redis_conn():
    try:
        r = redis.StrictRedis(
            host=Redis_HOST,
            port=Redis_PORT,
            password=Redis_PASSWORD,
            decode_responses=Redis_DECODE_RESPONSE,
            db=Redis_DB
        )
        return 1, r
    except Exception as e:
        return 0, ""


class RedisUtils:
    def __init__(self):
        self.key_template = Redis_KEY_TEMPLATE
        self.sts, self.rconn = redis_conn()

    def insert_val(self, ticket, val):
        mKey = '{}{}'.format(self.key_template, ticket)

        try:
            self.rconn.set(mKey, val)
            return 1
        except Exception as e:
            return 0

    def fetch_value(self, ticket):
        mKey = '{}{}'.format(self.key_template, ticket)
        payload = self.rconn.get(mKey)
        if payload:
            return 1, payload

        return 0, ''

    def delete_key(self, ticket):
        mKey = '{}{}'.format(self.key_template, ticket)
        self.rconn.delete(mKey)
        return 1

    def insert_value_expiary(self, key, suffix, payload, expiary):
        mKey = '{}{}_{}'.format(self.key_template, key, suffix)

        mPayload = json.dumps(payload, ensure_ascii=False, indent=1)
        if self.sts:
            return 1, self.rconn.setex(mKey,
                                       timedelta(seconds=expiary),
                                       value=mPayload
                                       )
        return 0, ''

    def fetch_value_expiary(self, key, suffix):
        mKey = '{}{}_{}'.format(self.key_template, key, suffix)
        if self.sts:
            try:
                payload = json.loads(self.rconn.get(mKey))
                return 1, payload
            except:
                return 0, ''
        return 0, ''

    def time_left(self, key, suffix):
        mKey = '{}{}_{}'.format(self.key_template, key, suffix)
        if self.sts:
            try:
                return self.rconn.pttl(mKey) / 1000
            except:
                return 0
        return 0

#
# ru = RedisUtils()
# key = 'umar'
# suffix = 'minute'
# expiary = 60
# payload = {'count': 1}
# ru.insert_value_expiary(key=key, suffix=suffix, expiary=expiary, payload=payload)
#
# time.sleep(10)
#
# timeleft = ru.time_left(key, suffix)
# print('Time Left:', timeleft)
# if timeleft:
#     payload = {'count': 5}
#     ru.insert_value_expiary(key=key, suffix=suffix, expiary=timeleft, payload=payload)
#     print (ru.time_left(key,suffix))

#
# payload = {'count':2}
