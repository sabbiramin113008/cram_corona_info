# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 31 Aug 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""

import uuid

import imgkit

from app_config import *


class ImageBuilder:
    def __init__(self):
        self.exe_file_path = Image_EXE_WKHTMLPDF_FILE_PATH
        self.config = imgkit.config(wkhtmltoimage=self.exe_file_path)
        self.output_base = Image_BASE_OUTPUT_DIR_PATH

    def build_summary(self, data):
        out_file_name = '{}_{}'.format('summary', str(uuid.uuid4()).replace('-', ''))
        out = '{}\\{}.jpg'.format(self.output_base, out_file_name)

        template = '''
        <h3>Date: {}</h3>
        
        <p>Confirmed Cases in Last 24 Hour: {}</p>
        <p>Death in Last 24 Hour: {}</p>
        <p>Recovery in Last 24 Hour: {}</p>
        <p>Cumulative Confirmed Cases: {}</p>
        <p>Cumulative Death: {}</p>
        <p>Cumulative Recovery: {}</p>
        <p>Daily Tests for COVID19: {}</p>
        <p>Confirmed new cases per day: {}</p>
        
        <h4>Total Tests for COVID19: {}</h4>
        
        '''.format(data.date,
                   data.confirmed_cases,
                   data.death_case,
                   data.recovery,
                   data.cumulative_confirmed_cases,
                   data.cumulative_death,
                   data.cumulative_recovery,
                   data.daily_test,
                   data.confirmed_new_cases_per_day,
                   data.total_tests,
                   )
        try:
            imgkit.from_string(template, out, config=self.config, )
            return 1, out_file_name
        except Exception as e:
            print('daily_summary: Error Building Image')
            return 0, ''

    def build_district_info(self, data):
        out_file_name = '{}_{}'.format('di', str(uuid.uuid4()).replace('-', ''))
        out = '{}\\{}.jpg'.format(self.output_base, out_file_name)
        template = '''
               <h3>District Name: {}</h3>
               <p>District Code: {}</p>
               
               
               <p>Total Cases: {}</p>
               <p>Total Death: {}</p>
               <p>Total Recover: {}</p>

               '''.format(
            data.name_en,
            data.district_code,
            data.cases,
            data.total_death,
            data.total_recover,
        )
        try:
            imgkit.from_string(template, out, config=self.config)
            return 1, out_file_name
        except Exception as e:
            print('district_info: Error Building Image')
            return 0, ''
