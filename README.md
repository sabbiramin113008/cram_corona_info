# Corona Info  
A RESTful Web API Service Application for Corona Info covering
   - Daily Summary
   - District Wise Summary

## Technologies Used:
1. Language- `Python  3.6.5` 
2. Micro Web Framework - `Flask`
3. Database Storage - `SQLite` and `MySQL` both.
4. In Memory Storage - `Redis`
5. WSL for Virtual Unix Tools.
6. Token Authentication - `JWT` Token Implementation.
   
## Application Features
1. Pattern Followed - `MVC`
2. Builder Patterns for Object Initiation.

## Features
1. API Endpoints for Serving Corona Information.
2. Global `Application Config` for easier Modification.
3. `Cron Job` for Fetching Data from `Cramstack API`s Periodically.
4. `API Rate Limit` implemented with `Redis`.
5. For `Daily Summary` and `Summary By District` are made to return images.
6. Get information filter by `date` and `district_code`.

# How To Run the application

## Pre Requisites. 
0. Download and install Python. 
   a. Global installing of packages used in this project is discouraged.
   b. Use Virtual Env for this project.
1. Install the dependencies from `requirements.txt`.
   a. `pip install -r requirements.txt` should work, if not, then 
   b. `pip install <package_name>` for all the pacakges.
2. Activate the virtual env. 
   
## Database Preparation.
1. Further db settings can be found under `./app_config.py` file.
2. For Schema Generation, use `./scripts/init_db.py`. 

## Preparation of Redis.
NB. Installing of Redis is beyond the scope. I've used WSL where Ubuntu is used as a host machine for Redis. 

1. Redis is used for `rate limit` API calls in this project.
2. If `locahost`,`port` and `db` are to be changed, then please follow `./app_config.py` file.

## Running the Cron Job
NB. Cron Jobs that fetches data periodically from the `Cramestack API Server`.

1. Open Terminal and run the `'/scripts/cron.py`, it will peridocally fetch the data from the api server.

## Installing `wkhtmlpdf.exe`

1. For Image Builder I've used [ImgKit](https://github.com/jarrekk/imgkit) which relies upon [wkhtmlpdf](https://wkhtmltopdf.org/).
2. For further installation, please follow along the download option there.
3. You might need to set the path of binary executable for `ImageBuilder` I implemented. Please, set `path` in `./app_config.py` file.

## Running the App

1. Make sure to Start `Redis Server` for Rate Limiter to work. 
2. `Rate Limit` is by default `ACTIVATED`, and can be changed in `./app_config.py` file.
3. Open any terminal and run `python server.py` from the root dir of this project.
Voila. 

## API Endpoints:
--------------------------------------
### 1. **Get Token - (Required if Rate Limit Status is `ACTIVATED`)**
If the Rate Limit is activate, it is required to acquire a `jwt` token from this API.
After a getting a successful token, that `token` is valid for a day and must be used 
at every request made to this server as `app_token` value in the header.

   - Method - `POST`
   - End Point - `/api/v1/token`

Sample API Path: `http://localhost:5656/api/v1/token`

**Request Body**
```json
{
   "mail":"john@doe.com"
}
```
**Response on Success**

```json
{
  "flag": "SUCCESS",
  "jwt_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJtYWlsIjoiam9obkBkb2UuY29tIiwiZXhwIjoxNTk5MTE5MTE4fQ.3t6jLWwZC8FRqNf3q8dWAp3MH5ZbDxwYyCk5cHaNNms"
}
```


### 2. **Get Summaries**
Returns the List of summaries throughout different dates.

   - `Method` - `GET`
   - `End Point` - `/api/v1/summaries`
   - 
Params - 
* `page` : Page No
* `size` : Size of contents

Sample API Path: `http://localhost:5656/api/v1/summaries?page=1&size=2`

Headers

```json
{
   "app_token":"<your-jwt-token>",
   "Content-Type":"application/json"
}
```

**Response Body**
```json
{
  "items": [
    {
      "confirmed_cases": 0,
      "confirmed_new_cases_per_day": 0,
      "cumulative_confirmed_cases": 0,
      "cumulative_death": 0,
      "cumulative_recovery": 0,
      "daily_test": 3,
      "date": "2020-03-04",
      "death_case": 0,
      "recovery": 0,
      "total_tests": 108
    },
    {
      "confirmed_cases": 0,
      "confirmed_new_cases_per_day": 0,
      "cumulative_confirmed_cases": 0,
      "cumulative_death": 0,
      "cumulative_recovery": 0,
      "daily_test": 3,
      "date": "2020-03-05",
      "death_case": 0,
      "recovery": 0,
      "total_tests": 111
    }
  ],
  "total_count": 2
}
```

### 3. **Get Summary By Date **
Returns Image of the Summary of the given `date`

   - `Method` - `GET`
   - `End Point` - `api/v1/dashboard/summary`
   
Params 
* `date` : Page No

Sample API Path: `http://localhost:5656/api/v1/dashboard/summary?date=2020-08-30`

Headers

```json
{
   "app_token":"<your-jwt-token>",
   "Content-Type":"application/json"
}
```
**Response**
![Get Summary By Date](./screenshots/summary_by_date.jpg)


### 4. **Get Summary By District Code **
Returns Image of the Summary of the given `district_code`

   - `Method` - `GET`
   - `End Point` - `api/v1/dashboard/district`
   - 
Params 

* `district_code` : District Code

Sample API Path: `http://localhost:5656/api/v1/dashboard/district?district_code=10`

Headers

```json
{
   "app_token":"<your-jwt-token>",
   "Content-Type":"application/json"
}
```
**Response**
![Get Summary By District Code](./screenshots/summary_by_district_code.jpg)

## Rate Limit
If `Rate_Limit_ACTIVATE` is `True` it will check the API calls per min and per day.

![Rate Limit in Action](./screenshots/rate_limit_per_min_quota_full.png)

## Comments:
1. Total 15 Hours have been spent for this project.
2. Many Production level implementations have been skipped due to very tight schedule. 
3. Several screenshots are added in `screenshots` dir.
4. At the time of the preparing documentation, I could not get data from the Cramstack Server. (Image added in `./screenshots`)