# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 31 Aug 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""

import os
from datetime import datetime

from peewee import *
from playhouse.pool import PooledMySQLDatabase, PooledSqliteDatabase

from Models.Configs import db_config
from app_config import *


def get_dbcontext():
    return PooledMySQLDatabase('{}'.format(db_config["db_name"]),
                               user=db_config["user"],
                               password=db_config["password"],
                               host=db_config["host"],
                               port=db_config["port"],
                               timeout=Database_TIMEOUT,
                               max_connections=Database_MAX_CONNECTION,
                               stale_timeout=Database_STALE_TIMEOUT)


def lite_db():
    DB_Path = os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        '{}.db'.format(db_config["db_name"])
    )
    return PooledSqliteDatabase(DB_Path,
                                timeout=10,
                                check_same_thread=False,
                                max_connections=Database_MAX_CONNECTION,
                                stale_timeout=Database_TIMEOUT)


app_db = get_dbcontext()


class BaseModel(Model):
    c_data = DateTimeField(default=datetime.now)
    u_data = DateTimeField(null=True)

    class Meta:
        database = app_db
