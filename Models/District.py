# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 31 Aug 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""
import codecs
import json

from Models.PrimeModels import *

file_path = 'res/districts.json'


class District(BaseModel):
    district_code = IntegerField(null=False, unique=True)
    name_en = CharField(null=False, unique=True)
    name_bn = CharField(null=False, unique=True)
    geo_loc = CharField(null=True)
    cases = IntegerField(default=0)
    total_death = IntegerField(null=True)
    total_recover = IntegerField(null=True)

    @classmethod
    def get_district_code(cls, name):

        resource_path = os.path.abspath(os.path.join(os.path.dirname(__file__), file_path))
        with codecs.open(resource_path, 'r', 'utf-8') as ff:
            districts = json.load(ff)
            return districts.get(name, None)

    @classmethod
    def insert_district_info(cls, data):
        for item in data:
            print (item)
            try:
                district = District(
                    district_code=cls.get_district_code(item.get('district_city_eng')),
                    name_en=item.get('district_city_eng'),
                    name_bn=item.get('district_city'),
                    geo_loc=item.get('lat_lon'),
                    cases=item.get('cases'),
                    total_death=item.get('total_death'),
                    total_recover=item.get('total_recover')
                )
                district.save()
                print('\tModel: {} is Saved'.format(item.get('district_city_eng')))
            except Exception as e:
                q = District.update(
                    district_code=cls.get_district_code(item.get('district_city_eng')),
                    name_en=item.get('district_city_eng'),
                    name_bn=item.get('district_city'),
                    geo_loc=item.get('lat_lon'),
                    cases=item.get('cases'),
                    total_death=item.get('total_death'),
                    total_recover=item.get('total_recover'),
                    u_data=datetime.now()
                ).where(District.name_en == item.get('district_city_eng'))
                q.execute()
                print('\tModel: {} is Updated'.format(item.get('district_city_eng')))

    @classmethod
    def get_response_by_dis_code(cls, dis_code):
        try:
            district = District.get(District.district_code == dis_code)
            return 1, district
        except DoesNotExist:
            return 0, ''
