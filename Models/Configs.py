# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 31 Aug 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""
from app_config import *

db_config = {
    'db_name': Datbase_NAME,
    'user': Database_USER,
    'password': Database_PASSWORD,
    'host': Database_HOST,
    'port': Database_PORT
}
