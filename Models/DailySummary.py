# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 31 Aug 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""

from Models.PrimeModels import *


class DailySummary(BaseModel):
    date = CharField(null=False, unique=True)
    confirmed_cases = IntegerField(default=0)
    death_case = IntegerField(default=0)
    recovery = IntegerField(default=0)
    cumulative_confirmed_cases = IntegerField(default=0)
    cumulative_death = IntegerField(default=0)
    cumulative_recovery = IntegerField(default=0)
    daily_test = IntegerField(default=0)
    confirmed_new_cases_per_day = IntegerField(default=0)
    total_tests = IntegerField(default=0)

    @classmethod
    def insert_dailySummary(cls, data):
        for item in data:
            try:
                dsmry = DailySummary(
                    date=item.get('Date'),
                    confirmed_cases=item.get('Confirmed_Cases_in_Last_24_Hour'),
                    death_case=item.get('Death_in_Last_24_Hour'),
                    recovery=item.get('Recovery_in_Last_24_Hour'),
                    cumulative_confirmed_cases=item.get('Cumulative_Confirmed_Cases'),
                    cumulative_death=item.get('Cumulative_Death'),
                    cumulative_recovery=item.get('Cumulative_Recovery'),
                    daily_test=item.get('Daily_Tests_for_COVID19_'),
                    confirmed_new_cases_per_day=item.get('Confirmed_new_cases_per_day'),
                    total_tests=item.get('Total_Tests_for_COVID19_'),
                )
                dsmry.save()
                print('{} is Saved:'.format(item.get('Date')))
            except Exception as e:
                print('{} is Already There:'.format(item.get('Date')))

    @classmethod
    def get_summary_paginated(cls, page, size):
        summaries = list()
        for summary in DailySummary.select().paginate(page, size):
            summaries.append(summary)
        return summaries

    @classmethod
    def get_summary_by_date(cls, date):
        try:
            summary = DailySummary.get(DailySummary.date == date)
            return 1, summary
        except DoesNotExist:
            return 0, ''
